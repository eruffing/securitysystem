/**
 * \addtogroup sensors Sensors
 * \brief Environmental sensors
 * \{
 * \file HallEffect.h
 *
 * \since 2015-11-25
 * \author Ethan Ruffing
 *
 * \brief Hall Effect sensor drivers
 *
 * This file provides drivers for the Honeywell SS41F Hall Effect Sensor.
 */ 

#include <avr/io.h>
#include "config.h"

#ifndef HALLEFFECT_H_
#define HALLEFFECT_H_

/**
 * Initializes the Hall Effect sensors.
 */
void initHallEffect();

/**
 * Determines if the Hall Effect sensor is in the latched state.
 *
 * \returns
 *     1 if the sensor is in the latched state, 0 if not.
 */
int hallEffectLatched();

#endif /* HALLEFFECT_H_ */

/**
 * \}
 */
