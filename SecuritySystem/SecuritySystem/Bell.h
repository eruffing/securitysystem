/**
 * \addtogroup alarm
 * \{
 * \file Bell.h
 *
 * \since 2015-11-25
 * \author Ethan Ruffing
 *
 * \brief Commands for using the physical bell
 *
 * This file provides utilities for driving a physical bell.
 */ 

#include <avr/io.h>
#include "config.h"

#ifndef BELL_H_
#define BELL_H_


/**
 * Sets up the output for driving the bell.
 */
void initBell();

/**
 * Changes the state of the bell as desired.
 *
 * \param[in] ringing
 *     Whether the bell should be ringing
 */
void switchBell(int ringing);

#endif /* BELL_H_ */

/**
 * \}
 */
