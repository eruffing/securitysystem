/**
 * \file Debounce.c
 *
 * \since 2015-09-11
 * \author Ethan Ruffing
 *
 * \brief Implementation of Debounce.h
 *
 * This file contains implementations of the functions declared in Debounce.h.
 */

#include "Debounce.h"

/**
 * @var The current debounced state of the switch.
 */
int debouncedKeyPress = 0;

void initSwitch()
{
	PIND |= _BV(SWITCH_PIN);	// Enable the pull-up resistor on the appropriate pin
}

void debounceSwitch(int *changed, int *pressed)
{
	static uint8_t Count = DEBOUNCE_TIME / 5;
	int rawState;
	*changed = 0;
	*pressed = !debouncedKeyPress;
	rawState = PIND & _BV(SWITCH_PIN);
	if (rawState == debouncedKeyPress) {
		Count = DEBOUNCE_TIME / 5;	// Reset timer
	} else {
		if (--Count <= 0) {
			debouncedKeyPress = rawState;
			*changed = 1;
			*pressed = debouncedKeyPress;
			Count = DEBOUNCE_TIME / 5;	// Reset timer
		}
	}
}
