/**
 * \file uart.h
 * \author Ethan Ruffing
 * \author Rob Bossemeyer
 * 
 * \brief Utilities for establishing a UART connection
 *
 * This file contains the necessary definitions for creating a UART connection
 * on the ATmega328P Xplained Mini board.
 * 
 * This file is based on code provided by Dr. Rob Bossemeyer of Grand Valley
 * State University.
 * 
 * To use, you must first <code>#define</code> the following values, *before
 * including this header*:
 *    - `F_CPU` - the cpu frequency (typically `16000000UL`)
 *    - `BAUD` - the desired baud rate (typically `9600`)
 *    - `MYUBRR` - the calculated baud rate (typically found as
 *      `F_CPU/16/BAUD-1`)
 * 
 * Then, in order to use the UART connection, you must call the following
 * to initalized the connection:
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.c}
 *     USART_Init(MYUBRR);
 *     stdout = &uart_output;
 *     stdin  = &uart_input;
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef UART_H_
#define UART_H_

void USART_Transmit(char c, FILE *stream);
char USART_Receive(FILE *stream);

void USART_Init(unsigned int ubrr);

FILE uart_output;
FILE uart_input;

#endif /* UART_H_ */
