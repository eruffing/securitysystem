/**
 * \file Debounce.h
 *
 * \since 2015-09-11
 * \author Ethan Ruffing
 *
 * \brief Utilities for debouncing a switch
 *
 * This file provides a set of tools to use for debouncing a switch. Before
 * using, be sure to call <code>initSwitch()</code> in order to initialize
 * proper hardware settings.
 */

#include <avr/io.h>

#ifndef DEBOUNCE_H_
#define DEBOUNCE_H_

/**
 * @def DEBOUNCE_TIME
 *     The time, in milliseconds, for which to debounce the switch. Must be
 *     a multiple of 5 in the range [20, 50].
 */
#define DEBOUNCE_TIME (50)

/**
 * @def SWITCH_PIN
 *     The pin to which the switch is attached. Note that this is assumed to be
 *     a position within the corresponding registers for PORTD.
 */
#define SWITCH_PIN (5)

/**
 * Activates the pull-up resistor for the switch.
 */
void initSwitch();

/**
 * Debounces the on-board switch. Adapted from the method
 * <code>DebounceSwitch1()</code> presented by Jack Ganssle on page 19 of his
 * "Guide to Debouncing".
 * 
 * \param[out] changed
 *     Whether the state of the switch has changed.
 * \param[out] pressed
 *     Whether the switch is currently pressed/on.
 */
void debounceSwitch(int *changed, int *pressed);


#endif /* DEBOUNCE_H_ */
