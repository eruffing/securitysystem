/**
 * \addtogroup communications
 * \{
 * \file TWI.h
 *
 * \since 2015-10-02
 * \author Ethan Ruffing
 * 
 * \brief Driver for TWI communications on the ATmega328P Xplained Mini Board.
 *
 * This file contains functions for use in communicating using the ATmega328P's
 * TWI system on the ATmega328P Xplained Mini Board.
 *
 * To use, you must first define the following preprocessor symbols, *before*
 * including this header:
 *    - `F_CPU` - the cpu frequency (typically `16000000UL`)
 *    - `F_SCL` - the TWI SCL frequency (a good value is `100000UL`)
 *
 * Note: This code is adapted from code provided by Bruce Wall in his article
 * "Add a DS1307 RTC clock to your AVR microcontroller" and from code provided
 * with the ATmel Application Note 315: "Using the TWI module as TWI master".
 */ 

#include <avr/io.h>

#ifndef TWI_H_
#define TWI_H_

/**
 * @def TW_READY
 *     Evaluates to true when the TWINT flag is set.
 */
#define TW_READY (TWCR & _BV(TWINT))

/**
 * @def TW_STATUS
 *     Returns the complete status code from the `TWSR` register.
 */
#define TW_STATUS (TWSR & 0xF8)

/**
 * Initializes the TWI connection settings.
 *
 * \param[in] f_cpu
 *     The CPU clock frequency.
 * \param[in] f_scl
 *     The frequency to use for the SCL line.
 */
void initTWIMaster(const unsigned long f_cpu, const unsigned long f_scl);

/**
 * Sends an TWI start signal.
 *
 * \return
 *     1 if successful, 0 if not.
 */
int twiSendStart();

/**
 * Sends an TWI stop signal.
 */
void twiSendStop();

/**
 * Sends a slave address.
 * 
 * \param[in] addr
 *     The address to send.
 * \return
 *     1 if successful, 0 if not. 
 */
int twiSendAddress(uint8_t addr);

/**
 * Sends a byte of data.
 * 
 * \param[in] data
 *     The byte to send.
 * \return
 *     1 if successful, 0 if not.
 */
int twiSendByte(uint8_t data);

/**
 * Reads a single byte of data over the TWI interface and does not respond with
 * ACK.
 *
 * \return
 *     The byte read.
 */
uint8_t twiReadNACK();

/**
 * Reads a single byte of data over the TWI interface and responds with an ACK.
 *
 * \return
 *     The byte read.
 */
uint8_t twiReadACK();

#endif /* TWI_H_ */

/**
 * \}
 */
