/**
 * \file Solenoid.c
 *
 * \since 2015-11-24
 * \author Ethan Ruffing
 *
 * \brief Implementation of Solenoid.h
 *
 * This file contains implementations of the functions declared in Solenoid.h.
 */ 

#include "Solenoid.h"

void initSolenoid() {
	SOL_DDR |= _BV(SOL_BIT);
	SOL_PORT &= ~_BV(SOL_BIT);
}

void switchSolenoid(uint8_t on) {
	if (on) {
		SOL_PORT |= _BV(SOL_BIT);
	} else {
		SOL_PORT &= ~_BV(SOL_BIT);
	}
}
