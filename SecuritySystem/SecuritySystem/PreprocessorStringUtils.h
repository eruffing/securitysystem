/**
 * \file PreprocessorStringUtils.h
 *
 * \since 2015-09-25
 * \author Ethan Ruffing
 * 
 * \brief A set of tools for manipulating strings in the preprocessor
 * 
 * This is a set of utilities that allows for more advanced string and name
 * manipulation in the preprocessor.
 */


#ifndef PREPROCESSORSTRINGUTILS_H_
#define PREPROCESSORSTRINGUTILS_H_

/**
 * @def _CONCAT(a,b)
 *     Concatenates the arguments a and b.
 *     
 *     This is based on a similar macro provided in a program by "Tushar"
 *     found at http://www.embedds.com/interfacing-matrix-keyboard-with-avr/
 */
#define _CONCAT(a,b) a##b

#endif /* PREPROCESSORSTRINGUTILS_H_ */
