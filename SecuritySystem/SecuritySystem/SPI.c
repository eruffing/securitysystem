/**
 * \file SPI.c
 *
 * \since 2015-10-09
 * \author Ethan Ruffing
 *
 * \brief Implementation of SPI.h
 *
 * This file implementations of the functions declared in SPI.h.
 * 
 * This program is adapted from that included with the AVR Application Note 151:
 * SPI on AVR, as well as from code by Michael Spiceland found at
 * https://github.com/mspiceland/avr-spiceduino-3310-thermistor.
 */ 

#include "SPI.h"

void initSPIMaster() {
	DDRB |= _BV(2) | _BV(3) | _BV(5);	// Set appropriate data directions

	// Enable SPI in master mode with /16 frequency and high-idle clock polarity
	SPCR = _BV(SPE) | _BV(MSTR) | _BV(SPR0);
}

void spiSendChar(char c) {
	SPDR = c;
	while (!(SPSR & _BV(SPIF)));
}
