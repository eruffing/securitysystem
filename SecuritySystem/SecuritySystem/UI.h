/**
 * \addtogroup display
 * \{
 * \file UI.h
 *
 * \since 2015-11-30
 * \author Ethan Ruffing
 *
 * \brief Functions for controlling the user interface
 *
 * This file provides functions for controlling the user interface.
 */ 

#include "RTC.h"
#include "LCD.h"
#include "Keypad.h"
#include "HallEffect.h"
#include "MotionDetector.h"
#include "PIN.h"
#include "Photocell.h"
#include "Log.h"
#include <avr/wdt.h>
#include <stdlib.h>
#include <stdio.h>
#include <util/delay.h>
#include <string.h>


#ifndef UI_H_
#define UI_H_

/**
 * A pointer to a function which accepts no parameters and returns void.
 */
typedef void (*func)(void);

/**
 * Prompts the user to input the current time and date, then sets the RTC
 * accordingly.
 */
void timeInput();

/**
 * Reads an integer of the specified length from the keypad.
 * 
 * \returns
 *     The integer that was read.
 */
uint16_t scand();

/**
 * Prompts the user to change the pin.
 */
void pinSet();

/**
 * Prompts the user to enter a pin and checks if the pin entered is correct.
 * 
 * \returns
 *     True if the pin entered is correct.
 */
uint8_t pinPrompt();

/**
 * Draws a menu to the screen.
 *
 * \param[in] title
 *     The title to display. (Should already be padded for centering, if
 *     desired).
 * \param[in] items
 *     The menu items to write. Each should be less than 11 characters in
 *     length.
 * \param[in] callbacks
 *     The functions to be called for each menu item.
 * \param[in] len
 *     The number of menu items to write.
 * \param[in] sel
 *     The index of the currently selected item.
 */
void menu(const char title[], const char * items[], func * callbacks, 
          uint8_t len, int * sel);

/**
 * Operates an interactive menu with the user.
 */
void mainMenu();

/**
 * Prints current status information to the first three rows of the LCD.
 */
void printStatus(uint8_t state);

/**
 * Moves the scrolling status display. Should be called periodically.
 */
void scrollStatus();

/**
 * Prints the formatted date and time to STDOUT.
 *
 * \param[in] hour
 *     The current hour
 * \param[in] minute
 *     The current minute
 * \param[in] second
 *     The current second
 * \param[in] date
 *     The current date
 * \param[in] month
 *     The current month
 * \param[in] year
 *     The current year
 */
void printDateTime(uint8_t hour, uint8_t minute, uint8_t second, uint8_t date,
                   uint8_t month, uint8_t year);

#endif /* UI_H_ */

/**
 * \}
 */
