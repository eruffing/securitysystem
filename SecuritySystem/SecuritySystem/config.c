/**
 * \file config.c
 *
 * \since 2015-12-09
 * \author Ethan Ruffing
 *
 * \brief Program configuration
 *
 * This file provides the pin mappings for all peripherals used on the system,
 * as well as other configuration parameters.
 */

#include "config.h"

uint16_t EEMEM eePin = 1234;
uint16_t EEMEM eeState = 0;
uint16_t EEMEM eeLastState = 0;
