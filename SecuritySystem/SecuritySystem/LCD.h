/**
 * \addtogroup display Display system
 * \brief Output functions and utilities
 * \details This module contains utilities for controlling and manipulating the
 * graphical output of the system.
 * \{
 * \file LCD.h
 *
 * \since 2015-10-09
 * \author Bashayer Alhanini
 * \author Ethan Ruffing
 *
 * \brief Driver for PCD8544 with ATmega328P
 *
 * This file provides a driver for using the PCD8544 LCD controller with a Nokia
 * 5110 LCD display with the ATmega328P microcontroller.
 * 
 * This program is adapted from code by Michael Spiceland found at
 * https://github.com/mspiceland/avr-spiceduino-3310-thermistor.
 */ 

#ifndef F_CPU
#error F_CPU must be defined before include LCD.h!
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include "SPI.h"
#include "Nokia5110Fonts.h"
#include "Timers.h"
#include "config.h"

#ifndef LCD_H_
#define LCD_H_

/**
 * @var cursorPos
 *     The current cursor position, in the form of an x-y coordinate.
 */
extern int cursorPos[2];

/**
 * Initializes the LCD display and necessary connections.
 */
void initLCD();

/**
 * Sends a command byte to the LCD.
 *
 * \param[in] command
 *     The byte to send.
 */
void lcdSendCommand(char command);

/**
 * Sends a data byte to the LCD.
 * 
 * \param[in] data
 *     The byte to send.
 */
void lcdSendData(char data);

/**
 * Sets the cursor location corresponding to basic font size.
 *
 * \param[in] x
 *     The x location (0 to 83)
 * \param[in] y
 *     The y location (0 to 5)
 * \return
 *     An error code. 0 if successful, 1 if x value is out of range, 2 if y
 *     value is out of range.
 */
int lcdSetCursor(uint8_t x, uint8_t y);

/**
 * Moves the cursor relative to its current location.
 *
 * \param[in] x
 *     The x distance to move
 * \param[in] y
 *     The y distance to move
 */
void lcdMoveCursor(int x, int y);

/**
 * Clears the LCD.
 */
void lcdClear();

/**
 * Clears a row on the LCD.
 *
 * \param[in] row
 *     The row to clear
 */
void lcdClearRow(uint8_t row);

/**
 * Flushes the current buffer to the LCD.
 */
void lcdUpdate();

/**
 * Writes a character to the current cursor position, then increments it.
 * 
 * \param[in] c
 *     The character to write.
 */
void lcdWriteChar(char c);

/**
 * Writes a character to the screen using the large font.
 *
 * \param[in] c
 *     The character to write.
 */
void lcdWriteCharLarge(char c);

/**
 * Writes a string to the LCD. Will be written from the current cursor position.
 *
 * \param[in] str
 *     The string to write to the screen.  Note: This MUST be a standard,
 *     NULL-TERMINATED string.
 */
void lcdWriteString(const char * str);

/**
 * Writes the specified number of characters from a string to the LCD. Will be
 * written from the current cursor position. The function will stop at either
 * the specified character, or the first NULL character encountered.
 * 
 * \param[in] str
 *     The string to write to the screen.
 * \param[in] n
 *     The index of the character to print up to (non-inclusive)
 */
void lcdWriteStringn(const char * str, int n);

/**
 * Writes a string to the LCD using the large font.
 *
 * \param[in] str
 *     The string to write.
 */
void lcdWriteStringLarge(const char * str);

/**
 * Writes an image to the LCD, starting at the current cursor position.
 *
 * \param[in] image
 *     The image to write. Must be a single-dimensional array of bytes, in
 *     horizontal fill order, with the correct number of elements to fill the
 *     remainder of the screen.
 */
void lcdWriteImage(const unsigned char * image);

#endif /* PCD8544_H_ */

/**
 * \}
 */
