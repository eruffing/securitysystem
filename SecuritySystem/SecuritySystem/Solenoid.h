/**
 * \addtogroup Solenoids
 * \brief Operate the solenoids
 * \{
 * \file Solenoid.h
 *
 * \since 2015-11-24
 * \author Ethan Ruffing
 *
 * \brief Solenoid driver
 *
 * This file contains utilities for operating a solenoid.
 */ 

#include <avr/io.h>
#include "AVRPreprocessorUtils.h"
#include "config.h"

#ifndef SOLENOID_H_
#define SOLENOID_H_

/**
 * Initializes the solenoid.
 */
void initSolenoid();

/**
 * Changes the state of the solenoid as specified.
 *
 * \param[in] on
 *     Whether the solenoid should be activated or deactivated.
 */
void switchSolenoid(uint8_t on);


#endif /* SOLENOID_H_ */

/**
 * \}
 */
