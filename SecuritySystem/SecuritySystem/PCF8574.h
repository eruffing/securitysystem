/**
 * \file PCF8574.h
 *
 * \since 2015-10-30
 * \author Ethan Ruffing
 *
 * \brief Driver for PCF8574 IO Expander
 *
 * This file provides a driver for the PCF8574 I/O Expander module. Be sure to
 * call `initTWIMaster()` (found in TWI.h, automatically included) before using
 * in order to initialize communications.
 */ 

#include "TWI.h"

#ifndef PCF8574_H_
#define PCF8574_H_

/**
 * @def PCF8574
 *     The TWI address of the PCF8574 chip.
 */
#define PCF8574 (0x40)

/**
 * Writes values to the IO expander chip.
 *
 * \param[in] data
 *     The values to write, where the MSB corresponds to P7 on the chip and the
 *     LSB corresponds to P0.
 */
void ioeWrite(uint8_t data);

/**
 * Writes the specified value to the specified bit.
 */
void ioeWriteBit(uint8_t bit, uint8_t val);

/**
 * Reads the input values from the IO expander chip.
 *
 * \return
 *     The values at the inputs on the IO expander, where the MSB of the return
 *     value corresponds to P7 on the chip and the LSB corresponds to P0.
 */
uint8_t ioeRead();

#endif /* PCF8574_H_ */
