/**
 * \file PCF8574.c
 *
 * \since 2015-10-30
 * \author Ethan Ruffing
 *
 * \brief Implementation of PCF8574.h
 *
 * This file contains implementations of the functions declared in PCF8574.h.
 */ 

#include "PCF8574.h"

/**
 * @var ioeBuffer
 *     Used to store the current state of the IO expander values.
 */
static uint8_t ioeBuffer;

void ioeWrite(uint8_t data) {
	twiSendStart();
	
	twiSendAddress(PCF8574);	// Send write mode address
	twiSendByte(data);	// Send data
	
	ioeBuffer = data;	// Update buffer
	
	twiSendStop();
}

void ioeWriteBit(uint8_t bit, uint8_t val) {
	val ? (ioeBuffer |= _BV(bit)) : (ioeBuffer &= ~_BV(bit));
	
	ioeWrite(ioeBuffer);
}

uint8_t ioeRead() {
	uint8_t data = 0;
	twiSendStart();
	
	twiSendAddress(PCF8574 | 0x01);
	data = twiReadNACK();
	
	twiSendStop();
	
	return data;
}
