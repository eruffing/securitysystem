/**
 * \file RGB_LED.c
 *
 * \since 2015-09-11
 * \author Ethan Ruffing
 *
 * \brief Implementation of RGB_LED.h
 * 
 * This file contains implementations of the functions declared in RGB_LED.h.
 */

#include "RGB_LED.h"

void initRgbLed() {
	// Set appropriate bits of DDRs for output
	RED_LED_DDR |= _BV(RED_LED_BIT);
	GREEN_LED_DDR |= _BV(GREEN_LED_BIT);
}

void rgbOn(char c) {
	switch (c) {
		case 'r':
			RED_LED_PORT &= ~_BV(RED_LED_BIT);
			break;
		case 'g':
			GREEN_LED_PORT &= ~_BV(GREEN_LED_BIT);
			break;
	}
}

void rgbOff(char c) {
	switch (c) {
		case 'r':
			RED_LED_PORT |= _BV(RED_LED_BIT);
			break;
		case 'g':
			GREEN_LED_PORT |= _BV(GREEN_LED_BIT);
			break;
	}
}

void rgbChange(char c) {
	rgbClear();
	rgbOn(c);
}

void rgbClear() {
	// Set appropriate bits of PORTD to HIGH
	RED_LED_PORT |= _BV(RED_LED_BIT);
	GREEN_LED_PORT |= _BV(GREEN_LED_BIT);
}
