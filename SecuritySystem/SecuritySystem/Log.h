/**
 * \file Log.h
 *
 * \since 2015-11-06
 * \author Ethan Ruffing
 * \author Bashayer Alhanini
 *
 * \brief Utilities for logging information to the EEPROM.
 *
 * This file contains utilities for logging information about alarm and
 * arm/disarm events to the EEPROM, as well as for reading those logs.
 *
 * Be sure to call `initLog()` before use.
 */ 

#include <avr/io.h>
#include <avr/eeprom.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "RTC.h"
#include "config.h"
#include "Alarm.h"
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>

#ifndef LOG_H_
#define LOG_H_

/**
 * @def START_CENTURY
 *     The century in which log entries can start. Note that this is to be the
 *     first two digits of the year in the century, *NOT* the ordinal number of
 *     the century (i.e., for the year 1994, this should be `19` for the 1900's,
 *     rather than `20` for twentieth century).
 */
#define START_CENTURY 20

/**
 * The type of event that a log entry is describing.
 */
typedef enum {
	/** A log entry for an alarm being triggered or cleared. */
	ALARM,
	/** A log entry for the system being armed or disarmed. */
	ARMDISARM
} entry_type;

/**
 * An entry in the log.
 */
typedef struct log_entry {
		/** The date and time of the entry */
		timestamp datetime;
		
		/**
		 * True if this is a positive action (as in system armed or alarm
		 * triggered); false if negative.
		 */
		uint8_t activated;
		
		/**
		 * The source of the alarm (if this is an alarm entry; NA if not).
		 */
		alarm_source source;
	} log_entry;

/**
 * Prepares for reading and writing entries in the log file by examining current
 * EEPROM contents.
 */
int initLog();

/**
 * Adds an event to the log.
 *
 * \param[in] year
 *     The two-digit year to log.
 * \param[in] month
 *     The month to store for the log entry (between 0 and 12).
 * \param[in] date
 *     The date in the month to store for the log entry (between 0 and 31).
 * \param[in] hour
 *     The hour to log for the event (between 0 and 23).
 * \param[in] minute
 *     The minute to log for the event (between 0 and 59).
 * \param[in] second
 *     The second to log for the event (between 0 and 59).
 * \param[in] type
 *     The type of event being logged.
 * \param[in] activated
 *     Whether is is an entry of the alarm being triggered (1) or cleared (0);
 *     or, if the type is `ARMDISARM`, whether the entry is of the system being
 *     armed (1) or disarmed (0).
 */
int writeLogEntry(uint8_t year, uint8_t month, uint8_t date, uint8_t hour,
                  uint8_t minute, uint8_t second, entry_type eType,
                  uint8_t activated);
			
/**
 * Adds an event to the log using the current date and time.
 *
 * \param[in] type
 *     The type of event being logged.
 * \param[in] activated
 *     Whether is is an entry of the alarm being triggered (1) or cleared (0);
 *     or, if the type is `ARMDISARM`, whether the entry is of the system being
 *     armed (1) or disarmed (0).
 */	  
void enterLog(entry_type eType, uint8_t activated, alarm_source source);

/**
 * Reads the log entry at the specified index.
 *
 * \param[in] index
 *     The index of the log entry to read, from 0 to 4, where 4 is the most
 *     recently logged item.
 * \param[in] eType
 *     The type of log entry this is
 * \param[out] entry
 *     A `log_entry` in which to place the data.
 */
void readLogEntry(uint8_t index, entry_type eType, log_entry * entry);
			 
/**
 * Dumps all data in the EEPROM locations.
 *
 * \param[out] lastChange
 *     The item used for tracking the current position in the log.
 * \param[out] data
 *     An array of `log_entry`s from the log.
 */
void dumpLogData(uint8_t* lastChange, log_entry * data);


#endif /* LOG_H_ */
