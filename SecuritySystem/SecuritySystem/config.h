/**
 * \addtogroup Configuration
 * \brief System configuration parameters
 * \{
 * \file config.h
 *
 * \since 2015-12-06
 * \author Ethan Ruffing
 *
 * \brief Program configuration
 *
 * This file provides the pin mappings for all peripherals used on the system,
 * as well as other configuration parameters.
 */ 

#include "AVRPreprocessorUtils.h"
#include <avr/eeprom.h>

#ifndef PINS_H_
#define PINS_H_

/**
 * @def PHOTOCELL_BIT
 *     The ADC bit to which the photocell output is connected.
 */
#define PHOTOCELL_BIT 2

/**
 * @def SOL_REG
 *     The register to which the solenoid is connected.
 * @def SOL_BIT
 *     The bit to which the solenoid is connected.
 */
#define SOL_REG D
#define SOL_BIT 1

/**
 * @def BELL_REG
 *     The register to which the bell is connected.
 * @def BELL_BIT
 *     The bit to which the bell is connected.
 */
#define BELL_REG D
#define BELL_BIT 0

/**
 * @def HE0_REG
 *     The register to which the first hall effect sensor is connected.
 * @def HE0_BIT
 *     The bit to which the first hall effect sensor is connected.
 * @def HE1_REG
 *     The register to which the second hall effect sensor is connected.
 * @def HE1_BIT
 *     The bit to which the second hall effect sensor is connected.
 */
#define HE0_REG D
#define HE0_BIT 3
#define HE1_REG D
#define HE1_BIT 2

// Keypad pins
/**
 * @def KEYPAD_COLUMNS_START
 *     The index of the first bit in the port register to which the keypad
 *     columns are connected. It is assumed that they increase sequentially from
 *     this value.
 * @def KEYPAD_ROWS_START
 *     The index of the first bit in the port register to which the keypad
 *     rows are connected. It is assumed that they increase sequentially from
 *     this value.
 */
#define KEYPAD_COLUMNS_START 0
#define KEYPAD_ROWS_START 3

/**
 * @def RED_LED_REG
 *     The letter of the register to which the red LED is connected.
 * @def GREEN_LED_REG
 *     The letter of the register to which the green LED is connected.
 * @def RED_LED_BIT
 *     The bit to which the red LED is connected.
 * @def GREEN_LED_BIT
 *     The bit to which the green LED is connected.
 */
#define RED_LED_REG C
#define GREEN_LED_REG C
#define RED_LED_BIT (0)
#define GREEN_LED_BIT (1)

/**
 * @def MOTIONDETCTOR_REG
 *     The register to which the motion detector is connected.
 * @def MOTIONDETECTOR_BIT
 *     The bit to which the motion detector is connected.
 */
#define MOTIONDETECTOR_REG C
#define MOTIONDETECTOR_BIT 3

/**
 * @def SCE_REG
 *     The register to which the LCD's SCE pin is connected.
 * @def SCE_BIT
 *     The bit to which the LCD's SCE bit is connected.
 * @def DC_REG
 *     The register to which the LCD's DC pin is connected.
 * @def DC_BIT
 *     The bit to which the LCD's DC pin is connected.
 * @def RST_REG
 *     The register to which the LCD's RST pin is connected.
 * @def RST_BIT
 *     The bit to which the LCD's RST pin is connected.
 */
#define SCE_REG D
#define SCE_BIT 6
#define DC_REG D
#define DC_BIT 7
#define RST_REG D
#define RST_BIT 4

/**
 * The location in EEPROM where the pin will be stored.
 */
extern uint16_t EEMEM eePin;
/**
 * The location in EEPROM where the current state will be stored.
 */
extern uint16_t EEMEM eeState;
/**
 * The location in EEPROM where the last state will be stored.
 */
extern uint16_t EEMEM eeLastState;

/**
 * @def LOG_LENGTH
 *     The number of log entries to maintain.
 */
#define LOG_LENGTH 5

/**
 * The permitted PIN length. Note that this has not yet been fully implemented.
 */
#define PIN_LENGTH 4

/* DO NOT MODIFY ANYTHING BELOW THIS LINE */
#define SOL_DDR DDR(SOL_REG)
#define SOL_PORT PORT(SOL_REG)
#define SOL_PIN PIN(SOL_REG)
#define BELL_DDR DDR(BELL_REG)
#define BELL_PORT PORT(BELL_REG)
#define BELL_PIN PIN(BELL_REG)
#define HE0_DDR DDR(HE0_REG)
#define HE0_PORT PORT(HE0_REG)
#define HE0_PIN PIN(HE0_REG)
#define HE1_DDR DDR(HE1_REG)
#define HE1_PORT PORT(HE1_REG)
#define HE1_PIN PIN(HE1_REG)
#define MOTIONDETECTOR_DDR DDR(MOTIONDETECTOR_REG)
#define MOTIONDETECTOR_PORT PORT(MOTIONDETECTOR_REG)
#define MOTIONDETECTOR_PIN PIN(MOTIONDETECTOR_REG)
#define RED_LED_PORT PORT(RED_LED_REG)
#define RED_LED_DDR DDR(RED_LED_REG)
#define GREEN_LED_PORT PORT(GREEN_LED_REG)
#define GREEN_LED_DDR DDR(GREEN_LED_REG)
#define SCE_PIN PIN(SCE_REG)
#define DC_PIN PIN(DC_REG)
#define RST_PIN PIN(RST_REG)
#define SCE_PORT PORT(SCE_REG)
#define DC_PORT PORT(DC_REG)
#define RST_PORT PORT(RST_REG)
#define SCE_DDR DDR(SCE_REG)
#define DC_DDR DDR(DC_REG)
#define RST_DDR DDR(RST_REG)

#endif /* PINS_H_ */

/**
 * \}
 */
