/**
 * \file LCD.c
 *
 * \since 2015-10-09
 * \author Bashayer Alhanini
 * \author Ethan Ruffing
 *
 * \brief Implementation of LCD.h.
 *
 * This file contains implementations of the functions declared in LCD.h.
 * 
 * This program is adapted from code by Michael Spiceland found at
 * https://github.com/mspiceland/avr-spiceduino-3310-thermistor.
 */ 

#include "LCD.h"

/**
 * @var lcdBuffer
 *     A buffer of pixels to print to the LCD.
 */
int cursorPos[] = {0, 0};
char lcdBuffer[8][84];

void initLCD() {
	
	// Set DDR values as appropriate
	SCE_DDR |= _BV(SCE_BIT);
	DC_DDR |= _BV(DC_BIT);
	RST_DDR |= _BV(RST_BIT);

	SCE_PORT &= ~_BV(SCE_BIT);	// Enable LCD
	RST_PORT &=~ _BV(RST_BIT);	// Reset LCD
	_delay_ms(100);
	RST_PORT |= _BV(RST_BIT);
	
	
	SCE_PORT |= _BV(SCE_BIT);	// Disable LCD
	
	lcdSendCommand(0x21);	// Choose extended instruction set
	lcdSendCommand(0x06);	// Set temperature coefficient
	lcdSendCommand(0xC8);	// Set Vop
	lcdSendCommand(0x13);	// LCD bias mode 1:48.
	lcdSendCommand(0x20);	// Return to normal instruction set	
	lcdSendCommand(0x08 | 0x04);	// Set normal display mode
	
	lcdClear();
}

void lcdSendCommand(char command) {
	SCE_PORT &= ~_BV(SCE_BIT);	// Enable LCD
	DC_PORT &= ~_BV(DC_BIT);	// Set LCD in command mode

	spiSendChar(command);
	
	SCE_PORT |= _BV(SCE_BIT);	// Disable LCD
}

void lcdSendData(char data) {
	SCE_PORT &= ~_BV(SCE_BIT);	// Enable LCD
	DC_PORT |= _BV(DC_BIT);	// Set LCD in data mode
	
	spiSendChar(data);

	SCE_PORT |= _BV(SCE_BIT);
}

int lcdSetCursor(uint8_t x, uint8_t y) {
	if (x > 83 || x < 0) {
		return 1;
	}
	if (y > 5 || y < 0) {
		return 2;
	}


	lcdSendCommand(0x80 | x);
	lcdSendCommand(0x40 | y);
	
	cursorPos[0] = x;
	cursorPos[1] = y;
	
	return 0;
}

void lcdMoveCursor(int x, int y) {
	lcdSetCursor(cursorPos[0] + x, cursorPos[1] + y);
}

void lcdClear() {
	int i0, i1;
	
	for (i0 = 0; i0 < 8; i0++) {
		for (i1 = 0; i1 < 90; i1++) {
			lcdSendData(0x00);

			if ((i0 < 6) && (i1 < 84)) {
				lcdBuffer[i0][i1] = 0x00;
			}
		}
	}
	
	lcdSetCursor(0, 0);
}


void lcdClearRow(uint8_t row) {
	int i;
	for (i = 0; i < 84; i++) {
		lcdBuffer[row][i] = 0x00;
	}
	
	lcdUpdate();
}

void lcdUpdate() {
	int i0, i1;
	
	lcdSetCursor(0, 0);
	
	for (i0 = 0; i0 < 7; i0 ++) {
		lcdSetCursor(0, i0);
		
		for (i1 = 0; i1 < 84; i1++) {
			lcdSendData(lcdBuffer[i0][i1]);
		}
	}
	
	lcdSetCursor(0, 0);
}

void lcdWriteChar(char c) {
	int i;
	
	if (c == '\n') {
		if (cursorPos[1] < 5) {
			lcdSetCursor(0, cursorPos[1] + 1);
		} else {
			lcdSetCursor(0, 0);
		}
		return;
	}

	lcdBuffer[cursorPos[1]][cursorPos[0]] = 0x00;
	
	for (i = 0; i < 5; i++) {
		lcdBuffer[cursorPos[1]][cursorPos[0] + i] |=
			pgm_read_byte(&(smallFont[(c-32) * 5 + i]));
	}
	
	lcdBuffer[cursorPos[1]][cursorPos[0] + 6] = 0x00;

	for (i = 0; i < 7; i++) {
		lcdSendData(lcdBuffer[cursorPos[1]][cursorPos[0]++]);
	}
}

void lcdWriteCharLarge(char c) {
	int i0, i1, i2, i3;
	
	for (i0 = 0; i0 < 5; i0++) {
		for (i1 = 0; i1 < 4; i1 ++) {
			uint16_t orig = pgm_read_byte(&(smallFont[(c - 32) * 5 + i0]));
			unsigned char new0 = 0, new1 = 0;
			
			for (i2 = 0; i2 < 4; i2++) {
				for (i3 = 0; i3 < 2; i3++) {
					new0 |= (orig & _BV(i2)) << (i2 + i3); 
					new1 |= (orig & _BV(i2 + 4)) >> 4 << (i3 + i2);
				}
			}			

			lcdBuffer[cursorPos[1]][cursorPos[0] + i0 * 4 + i1] |= new0;
			lcdBuffer[cursorPos[1] + 1][cursorPos[0] + i0 * 4 + i1] |= new1;
		}
	}
	
	lcdUpdate();
}

void lcdWriteString(const char * str) {
	int i = 0;
	char c;
	
	for (c = str[i]; c != '\0'; c = str[++i]) {
		lcdWriteChar(c);
	}	
}

void lcdWriteStringn(const char * str, int n) {
	int i = 0;
	char c;
	
	for (c = str[i]; c != '\0' && i < n; c = str[++i]) {
	lcdWriteChar(c);
	}
}

void lcdWriteStringLarge(const char * str) {
	int i = 0, cursorx, cursory;
	char c;
	for (c = str[i]; c != 0; c = str[++i]) {
		cursorx = cursorPos[0];
		cursory = cursorPos[1];
		
		lcdWriteCharLarge(c);
		
		if (cursorx + 22 > 83) {
			cursorx = 0;
			
			if (cursory + 1 > 5) {
				cursory = 0;
			} else {
				cursory ++;
			}
		} else {
			cursorx += 22;
		}
		
		lcdSetCursor(cursorx, cursory);
	}
}

void lcdWriteImage(const unsigned char * image) {
	int i0 = cursorPos[1], i1 = cursorPos[0];
	int n = 0;
	
	for (i0 = cursorPos[1]; i0 < 6 && n < 6; i0++) {
		for (i1 = cursorPos[0]; i1 < 84 && n < 84; i1++) {
			lcdBuffer[i0][i1] = image[i0 * 84 + i1];
			n++;
		}
	}
	
	lcdUpdate();
}
