/**
 * \addtogroup rtc
 * \{
 * \file DS3231Registers.h
 *
 * \since 2015-10-02
 * \author Ethan Ruffing
 *
 * \brief Register definitions for the DS3231 RTC module.
 *
 * This file contains macro definitions for the registers in the DS3231 Real
 * Time Clock (RTC) module, as defined in the module's data sheet.
 */ 


#ifndef DS3231REGISTERS_H_
#define DS3231REGISTERS_H_

/**
 * @def RTCS
 *     The seconds register of the DS3231 module.
 * @def RTCMN
 *     The minutes register of the DS3231 module.
 * @def RTCHR
 *     The hour register of the DS3231 module.
 * @def RTCDW
 *     The day of the week register of the DS3231 module.
 * @def RTCDT
 *     The date register of the DS3231 module.
 * @def RTCMO
 *     The month register of the DS3231 module.
 * @def RTCYR
 *     The year register of the DS3231 module.
 */
#define RTCS (0x00)
#define RTCMN (0x01)
#define RTCHR (0x02)
#define RTCDW (0x03)
#define RTCDT (0x04)
#define RTCMO (0x05)
#define RTCYR (0x06)

/**
 * @def RTCCR
 *     The control register of the DS3231 module.
 * @def RTCSR
 *     The status and second control register of the DS3231 module.
 */
#define RTCCR (0x0E)
#define RTCSR (0x0F)

/**
 * @def RTCTRH
 *     The upper bits of the temperature register of the DS3231 module.
 * @def RTCTRL
 *     The lower bits of the temperature register of the DS3231 module.
 */
#define RTCTRH (0x11)
#define RTCTRL (0x12)


#endif /* DS3231REGISTERS_H_ */

/**
 * \}
 */
