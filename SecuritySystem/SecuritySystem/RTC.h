/**
 * \addtogroup rtc Real-time Clock
 * \brief Utilities for interacting with the real-time clock module.
 * \{
 * \file RTC.h
 *
 * \since 2015-10-02
 * \author Ethan Ruffing
 *
 * \brief Driver for DS3231 for ATmega328P
 * 
 * This file provides a driver for using the DS3231 Real Time Clock (RTC) module
 * with the ATmega328P.
 *
 * To use, you must first define the following symbols, *before* including this
 * header:
 *    - `F_CPU` - the cpu frequency (typically `16000000UL`)
 *    - `F_SCL` - the TWI SCL frequency (a good value is `100000UL`)
 *
 * A note for reference: the SCL pin on the ATmega328P is PC5, and the SDA pin
 * is PC4.
 *
 * Note: This code is adapted from code provided by Bruce Wall in his article
 * "Add a DS1307 RTC clock to your AVR microcontroller"
 */ 

#include <avr/io.h>

#include "TWI.h"
#include "DS3231Registers.h"

#ifndef RTC_H_
#define RTC_H_

/**
 * @def DS3231
 *     The TWI address of the DS3231 module.
 */
#define DS3231 (0xD0)

/**
 * A timestamp data type.
 */
typedef struct timestamp {
		/** The four-digit year */
		uint16_t year;
		/** The two-digit month (from 1 to 12) */
		uint8_t month;
		/** The two-digit date (from 1 to 31) */
		uint8_t date;
		/** The two-digit hour (from 0 to 24) */
		uint8_t hour;
		/** The two-digit minute (from 0 to 60) */
		uint8_t minute;
		/** The two-digit second (from 0 to 60) */
		uint8_t second;
	} timestamp;

/**
 * Writes data to a register in the RTC module.
 * 
 * \param[in] rtcRegister
 *     The internal address of the register on the RTC to which the data will be
 *     written.
 * \param[in] data
 *     The data to write to the RTC.
 */
void rtcWriteRegister(uint8_t rtcRegister, uint8_t data);

/**
 * Reads data from a register on the RTC module.
 * 
 * \param[in] rtcRegister
 *     The internal address of the register on the RTC to read.
 * \return
 *     The data from the specified address on the RTC.
 */
uint8_t rtcReadRegister(uint8_t rtcRegister);

/**
 * Reads the time from the RTC.
 * 
 * \param[out] hour
 *     The current hour
 * \param[out] minute
 *     The current minute
 * \param[out] second
 *     The current second
 */
void rtcReadTime(uint8_t *hour, uint8_t *minute, uint8_t *second);

/**
 * Reads the date from the RTC.
 *
 * \param[out] date
 *     The current date
 * \param[out] month
 *     The current month
 * \param[out] year
 *     The current year
 */
void rtcReadDate(uint8_t *date, uint8_t *month, uint16_t *year);

/**
 * Reads the date and time from the RTC.
 *
 * \param[out] hour
 *     The current hour
 * \param[out] minute
 *     The current minute
 * \param[out] second
 *     The current second
 * \param[out] date
 *     The current date
 * \param[out] month
 *     The current month
 * \param[out] year
 *     The current year
 */
void rtcReadDateTime(uint8_t *hour, uint8_t *minute, uint8_t *second,
                     uint8_t *date, uint8_t *month, uint16_t *year);

/**
 * Reads the date and time from the RTC.
 * 
 * \returns
 *     The current date and time
 */ 
timestamp rtcRead();

/**
 * Sets the time on the RTC module.
 *
 * \param[in] hour
 *     The current hour
 * \param[in] minute
 *     The current minute
 * \param[in] second
 *     The current second
 */
void rtcSetTime(uint8_t hour, uint8_t minute, uint8_t second);

/**
 * Sets the date on the RTC module.
 *
 * \param[in] date
 *     The current date
 * \param[in] month
 *     The current month
 * \param[in] year
 *     The current year
 */
void rtcSetDate(uint8_t date, uint8_t month, uint8_t year);

/**
 * Sets the date and time on the RTC module.
 *
 * \param[in] hour
 *     The current hour
 * \param[in] minute
 *     The current minute
 * \param[in] second
 *     The current second
 * \param[in] date
 *     The current date
 * \param[in] month
 *     The current month
 * \param[in] year
 *     The current year
 */
void rtcSetDateTime(uint8_t hour, uint8_t minute, uint8_t second, uint8_t date,
                    uint8_t month, uint8_t year);
					
/**
 * Reads the temperature from the DS3231 module.
 * 
 * \return
 *     The current temperature, in degrees Celsius.
 */
float rtcReadTemperature();


#endif /* RTC_H_ */

/**
 * \}
 */
