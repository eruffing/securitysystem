/**
 * \file RTC.c
 *
 * \since 2015-10-02
 * \author Ethan Ruffing
 *
 * \brief Implementation of RTC.h
 * 
 * This file contains implementations of the functions declared in RTC.h.
 *
 * Note: This code is adapted from code provided by Bruce Wall in his article
 * "Add a DS1307 RTC clock to your AVR microcontroller"
 */

#include "RTC.h"

void rtcWriteRegister(uint8_t rtcRegister, uint8_t data) {
	twiSendStart();
	
	twiSendAddress(DS3231);
	twiSendByte(rtcRegister);
	twiSendByte(data);
	
	twiSendStop();
}

uint8_t rtcReadRegister(uint8_t rtcRegister) {
	uint8_t data = 0;
	twiSendStart();
	twiSendAddress(DS3231);
	twiSendByte(rtcRegister);
	twiSendStop();
	
	twiSendStart();
	twiSendAddress(DS3231 + 1);
	
	data = twiReadNACK();
	
	twiSendStop();
	
	return data;
}

void rtcReadTime(uint8_t *hour, uint8_t *minute, uint8_t *second) {
	*hour = rtcReadRegister(RTCHR);
	if (*hour & _BV(6)) {
		*hour = (*hour & 0x0f) + ((*hour & _BV(5)) ? 12 : 0)
		        + ((*hour & _BV(4)) ? 10 : 0);
	} else {
		*hour = (*hour & 0x0f) + ((*hour & _BV(5)) ? 20 : 0) 
		        + ((*hour & _BV(4)) ? 10 : 0);
	}
	*minute = rtcReadRegister(RTCMN);
	*minute = (*minute & 0x0F) + 10 * ((*minute & 0x70) >> 4);
	*second = rtcReadRegister(RTCS);
	*second = (*second & 0x0F) + 10 * ((*second & 0x70) >> 4);
}

void rtcReadDate(uint8_t *date, uint8_t *month, uint16_t *year) {
	*date = rtcReadRegister(RTCDT);
	*date = (*date & 0x0F) + 10 * ((*date & 0x30) >> 4);
	*month = rtcReadRegister(RTCMO);
	*month = (*month & 0x0F) + 10 * ((*month & 0x10) >> 4);
	*year = rtcReadRegister(RTCYR);
	*year = (*year & 0x0F) + 10 * ((*year & 0xF0) >> 4);
	*year += 2000;
}

void rtcReadDateTime(uint8_t *hour, uint8_t *minute, uint8_t *second,
                     uint8_t *date, uint8_t *month, uint16_t *year) {
	rtcReadTime(hour, minute, second);
	rtcReadDate(date, month, year);
}

timestamp rtcRead() {
	timestamp time;
	rtcReadDateTime(&time.hour, &time.minute, &time.second, &time.date,
	                &time.month, &time.year);
	return time;
}

void rtcSetTime(uint8_t hour, uint8_t minute, uint8_t second) {
	uint8_t hourreg = hour % 10;
	if (hour > 20) {
		hourreg |= _BV(5);
	} else if (hour > 10) {
		hourreg |= _BV(4);
	}
	rtcWriteRegister(RTCHR, hourreg);
	rtcWriteRegister(RTCMN, ((minute / 10) << 4) | (minute % 10));
	rtcWriteRegister(RTCS, ((second / 10) << 4) | (second % 10));
}

void rtcSetDate(uint8_t date, uint8_t month, uint8_t year) {
	rtcWriteRegister(RTCDT, ((date / 10) << 4) + (date % 10));
	rtcWriteRegister(RTCMO, ((month / 10) << 4) + (month % 10));
	rtcWriteRegister(RTCYR, ((year / 10) << 4) + (year % 10));
}

void rtcSetDateTime(uint8_t hour, uint8_t minute, uint8_t second, uint8_t date,
                    uint8_t month, uint8_t year) {
	rtcSetTime(hour, minute, second);
	rtcSetDate(date, month, year);
}

float rtcReadTemperature() {
	float temp = rtcReadRegister(RTCTRH);
	temp += (rtcReadRegister(RTCTRL) >> 6) * 0.25;
	
	return temp;
}
