/**
 * \file Keypad.c
 *
 * \since 2015-09-25
 * \author Ethan Ruffing
 * 
 * \brief Implementation of Keypad.h
 *
 * This file contains implementations of the functions declared in Keypad.h.
 */

#include "Keypad.h"

void initKeypad() {
	ioeWrite( 0xFF );
}

char getKey() {	
	cli();
	uint8_t i0, i1, pressed;
	ioeWrite(0xFF);
	
	for (i0 = 0; i0 < 3; i0++) {
		ioeWriteBit(KEYPAD_COLUMNS_START + i0, 0);
		_delay_ms(2);
		
		pressed = ioeRead();
		
		for (i1 = 0; i1 < 4; i1++) {
			if (!(pressed & _BV(KEYPAD_ROWS_START + i1))) {
				//while (!(ioeRead() & _BV(KEYPAD_ROWS_START + i1)));
				ioeWriteBit(KEYPAD_COLUMNS_START + i0, 1);
				return indexToChar(i1, 2-i0);
			}
		}
		ioeWriteBit(KEYPAD_COLUMNS_START + i0, 1);
	}
	sei();
	return '\0';
}

char getInput() {
	cli();
	char c = getKey();
	while (c == '\0') {
		c = getKey();
		wdt_reset();
	}
	sei();
	return c;
}

char indexToChar(int row, int column) {
	if (row < 0 || column < 0 || row > 3 || column > 2) {
		return '\0';
	} else {
		return pgm_read_byte(&(keypad[row][column]));
	}
}
