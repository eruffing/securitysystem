/**
 * \file Log.c
 *
 * \since 2015-11-06
 * \author Ethan Ruffing
 * \author Bashayer Alhanini
 *
 * \brief Implementation of Log.h
 *
 * This file contains implementations of the functions declared in Log.h.
 */

#include "Log.h"

/**
 * @var lastChanged
 *     The address of the last changed log entry in the EEPROM.
 */
static uint8_t lastChanged = 0;
uint8_t EEMEM eeLastChanged = 0;

/**
 * @var logCount
 *     The number of entries stored in the log.
 */
static uint8_t logCount = 0;
uint8_t EEMEM eeLogCount = 0;

/**
 * The alarm log entries.
 */
log_entry EEMEM alarmLog[LOG_LENGTH];

/**
 * The arm/disarm log entries
 */
log_entry EEMEM armDisarmLog[LOG_LENGTH];

/**
 * Determines the actual location of an index in the log.
 *
 * \param[in] logIndex
 *     The index of the log entry to read, from 0 to 4, where 0 is the most
 *     recently logged item.
 * \return
 *     The actual index of the log in the EEPROM.
 */
uint8_t logAcutalLocation(uint8_t logIndex) {
	uint8_t i0, converted = lastChanged;
	for (i0 = logIndex; i0 >= 0; i0--) {
		converted = (converted == 0) ? LOG_LENGTH - 1 : converted - 1;
	}
	return converted;
}

int initLog() {
	lastChanged = eeprom_read_byte(&eeLastChanged);
	logCount = eeprom_read_byte(&eeLogCount);
	
	return logCount;
}

int writeLogEntry(uint8_t year, uint8_t month, uint8_t date, uint8_t hour,
                  uint8_t minute, uint8_t second, entry_type eType,
                  uint8_t activated) {
	log_entry entry;	// Temporary space for building the entry
	
	// Build the entry
	timestamp datetime;
	datetime.year = year;
	datetime.month = month;
	datetime.date = date;
	datetime.hour = hour;
	datetime.minute = minute;
	datetime.second = second;
	entry.datetime = datetime;
	entry.activated = activated;
	
	// Update lastChanged
	lastChanged = lastChanged >= LOG_LENGTH ? 1 : lastChanged + 1;
	if (logCount < LOG_LENGTH) {
		logCount++;
	}
	eeprom_update_byte(&eeLastChanged, lastChanged);
	eeprom_update_byte(&eeLogCount, logCount);
	
	// Write the constructed entry to EEPROM
	eeprom_update_block(&entry, (eType == ALARM ? &(alarmLog[lastChanged]) :
	                             &(armDisarmLog[lastChanged])),
						 sizeof(log_entry));
	
	return logCount;
}

void enterLog(entry_type eType, uint8_t activated, alarm_source source) {
	log_entry entry;
	entry.datetime = rtcRead();
	entry.activated = activated;
	
	// Update lastChanged
	lastChanged = lastChanged >= LOG_LENGTH ? 1 : lastChanged + 1;
	if (logCount < LOG_LENGTH) {
		logCount++;
	}
	eeprom_update_byte(&eeLastChanged, lastChanged);
	eeprom_update_byte(&eeLogCount, logCount);
	
	// Write the constructed entry to EEPROM
	eeprom_update_block(&entry, (eType == ALARM ? &(alarmLog[lastChanged]) :
	                             &(armDisarmLog[lastChanged])),
	                    sizeof(log_entry));
}

void readLogEntry (uint8_t index, entry_type eType, log_entry * entry) {
	if (eType == ALARM) {
		eeprom_read_block(entry, &alarmLog[index], sizeof(log_entry));
	} else {
		eeprom_read_block(entry, &armDisarmLog[index], sizeof(log_entry));
	}
}

void dumpLogData(uint8_t* lastChange, log_entry * data) {
	*lastChange = eeprom_read_byte(&eeLastChanged);
	uint8_t i;
	for (i = 0; i < LOG_LENGTH; i ++) {
		eeprom_read_block(&data[i], &alarmLog[i], sizeof(log_entry));
	}
}
