/**
 * \addtogroup sensors
 * \{
 * \file MotionDetector.h
 *
 * \since 2015-11-24
 * \author Ethan Ruffing
 * 
 * \brief Motion detector driver
 *
 * This file provides tools for using the PIR motion sensor.
 */ 

#include <avr/io.h>
#include "AVRPreprocessorUtils.h"
#include "config.h"

#ifndef MOTIONDETECTOR_H_
#define MOTIONDETECTOR_H_

/**
 * Initializes the motion detector.
 */
void initMotionDetector();

/**
 * Determines if the motion detector is currently outputting a HIGH.
 *
 * \return
 *     1 if motion is detected, 0 if not.
 */
int motionDetected();

#endif /* MOTIONDETECTOR_H_ */

/**
 * \}
 */
