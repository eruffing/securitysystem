/**
 * \file MotionDetector.c
 *
 * \since 2015-11-24
 * \author Ethan Ruffing
 *
 * \brief Implementation of MotionDetector.h
 *
 * This file contains implementations of the functions declared in
 * `MotionDetector.h`.
 */ 

#include "MotionDetector.h"

void initMotionDetector() {
	MOTIONDETECTOR_DDR &= ~_BV(MOTIONDETECTOR_BIT);
	PCICR |= _BV(PCIE1);
	PCMSK1 |= _BV(MOTIONDETECTOR_BIT);
}

int motionDetected() {
	return !(!(MOTIONDETECTOR_PIN & _BV(MOTIONDETECTOR_BIT)));
}
