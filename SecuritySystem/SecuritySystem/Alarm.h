/**
 * \addtogroup alarm Alarm system
 * \brief Tools for activating an alarm
 * \{
 * \file Alarm.h
 *
 * \since 2015-12-06
 * \author Ethan Ruffing
 *
 * This file provides a system for running an alarm which activates and
 * deactivates a bell and a red LED on one-second intervals. To use the alarm,
 * first activate the alarm by calling `switchAlarm(1)`. Then, on regular
 * intervals, call `alarmToggle()` in order to make the bell and red LED turn
 * on and off.
 */ 

#include "Bell.h"
#include "RGB_LED.h"
#include <avr/interrupt.h>

#ifndef ALARM_H_
#define ALARM_H_

/**
 * An enum for the source of an alarm.
 */
typedef enum {
		NA,
		/** Alarm activated by motion sensor */
		MOTION,
		/** Fire alarm */
		FIRE,
		/** Alarm activated by door opening */
		DOOR,
		/** Alarm activated by window opening */
		WINDOW
	} alarm_source;

/**
 * Activates or deactivates the alarm system by beginning (or stopping)
 * one-second intervals of the bell ringing and the red LED lighting.
 *
 * \param[in] on
 *     Whether the alarm should be on or off.
 * \returns
 *     Whether the alarm is now on or off.
 */
uint8_t switchAlarm(uint8_t on);

/**
 * Should be called by the alarm's ISR. Switches the bell and red LED on and off
 * in order to create a more realistic alarm sound.
 */
void alarmToggle();

#endif /* ALARM_H_ */

/**
 * \}
 */
