/**
 * \file Alarm.c
 *
 * \since 2015-12-06
 * \author Ethan Ruffing
 *
 * \brief Implementation of Alarm.h.
 *
 * This file contains implementations of the functions declare din Alarm.h.
 */ 

#include "Alarm.h"

uint8_t _alarmIsOn = 0;
uint8_t _alarmState = 0;

uint8_t switchAlarm(uint8_t on) {
	if (on) {
		_alarmIsOn = 1;
		switchBell(1);
		rgbOn('r');
		_alarmState = 1;
	} else {
		_alarmIsOn = 0;
		switchBell(0);
		rgbOff('r');
		_alarmState = 0;
	}
	
	return _alarmIsOn;
}

void alarmToggle() {
	if (_alarmState) {
		switchBell(0);
		rgbOff('r');
		_alarmState = 0;
	} else {
		switchBell(1);
		rgbOn('r');
		_alarmState = 1;
	}
}
