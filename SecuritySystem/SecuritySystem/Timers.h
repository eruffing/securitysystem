/**
 * \file Timers.h
 *
 * \since 2015-09-30
 * \author Ethan Ruffing
 * 
 * \brief Timer definitions for use in the home security system.
 *
 * To use, you must first define the following preprocessor symbols:
 *    - `F_CPU` - the cpu frequency (typically `16000000UL`)
 *
 * This file contains functions for use in setting up and using the various
 * built-in timers in the ATmega328P for use in the home security system.
 */

#ifndef F_CPU
#error F_CPU *must* be defined before including Timers.h.
#endif

#include <avr/io.h>

#ifndef TIMERS_H_
#define TIMERS_H_

/**
 * @def DEFAULT_DUTYCYCLE
 *     The default duty cycle to use for the PWM output.
 */
#define DEFAULT_DUTYCYCLE (70)

/**
 * Sets up Timer0 for use in generating a 200 Hz PWM signal.
 */
void initTimer0();

/**
 * Sets the duty cycle for the PWM signal.
 *
 * \param[in] dutyCycle
 *     The duty cycle to set, as an integer representation of the percentage.
 */
void setT0DutyCycle(uint8_t dutyCycle);

/**
 * Sets up Timer1 for executing tasks on 500 ms intervals.
 */
void initTimer1();

/**
 * Sets up Timer2 for use in executing periodic tasks.
 */
void initTimer2();


#endif /* TIMERS_H_ */
