/**
 * \file SecuritySystem.c
 * \author Ethan Ruffing
 * \author Bashayer Alhanini
 * \since 2015-10-19
 *
 * \brief Main file for security system project.
 *
 * This is the main file for the home security system project.
 */

#ifndef F_CPU
#error F_CPU must be defined.
#endif
#define BAUD 9600
#define MYUBRR F_CPU/16/BAUD-1
#define F_SCL 100000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include "TWI.h"
#include "Keypad.h"
#include "PIN.h"
#include "RTC.h"
#include "Photocell.h"
#include "LCD.h"
#include "Timers.h"
#include "RGB_LED.h"
#include "MotionDetector.h"
#include "Solenoid.h"
#include "HallEffect.h"
#include "Bell.h"
#include "UI.h"
#include "Alarm.h"
#include "Log.h"
#include "Watchdog.h"
#include <util/delay.h>

ISR (SPM_READY_vect) {
	return;
}

volatile uint16_t state = 0;
volatile uint16_t laststate = 0;
volatile uint8_t inputting = 0;
volatile uint8_t count = 0;

/**
 * Safely changes the state variable.
 *
 * \param[in] newState
 *     The new state to set.
 */
void setState(uint16_t newState) {
	laststate = state;
	state = newState;
	eeprom_update_word(&eeLastState, laststate);
	eeprom_update_word(&eeState, state);
	wdt_reset();
}

uint8_t _alarmOn = 0;

ISR(TIMER1_COMPA_vect) {
	// Adjust back lighting on every cycle
	float v = readVoltage();
	int dutyCycle = 100 - v * 25;	// Calculate duty cycle based on voltage
	dutyCycle = dutyCycle < 0 ? 0 : dutyCycle;;
	dutyCycle = dutyCycle > 100 ? 100 : dutyCycle;
	setT0DutyCycle(dutyCycle);
	
	if (state != 5 && !inputting) {
		lcdClear();
		printStatus(state);
	}
	
	scrollStatus();
	
	if (_alarmOn) {
		alarmToggle();
	}
	
	if (rtcReadTemperature() > 43.3333) {
		enterLog(ALARM, 1, FIRE);
		_alarmOn = switchAlarm(1);
		setState(99);
	}
	
	if (!state || state == 5) {
		rgbOn('g');
	}
	
	TCNT1 = 0;
}

ISR(PCINT1_vect) {
	if (state == 1) {
		if (MOTIONDETECTOR_PIN & _BV(MOTIONDETECTOR_BIT)) {
			enterLog(ALARM, 1, MOTION);
			_alarmOn = switchAlarm(1);
			setState(2);
		}
	}
}

ISR(PCINT2_vect) {
	if (state == 1) {
		if (HE0_PIN & _BV(HE0_BIT)) {
			enterLog(ALARM, 1, DOOR);
			_alarmOn = switchAlarm(1);
			setState(3);
		} else if (HE1_PIN & _BV(HE1_BIT)) {
			enterLog(ALARM, 1, WINDOW);
			_alarmOn = switchAlarm(1);
			setState(4);
		}
	}
}

void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));
uint8_t status __attribute__((section(".noinit")));

void wdt_init(void)
{
	status = MCUSR;
	MCUSR = 0;
	wdt_disable();
	
	return;
}

int main() {
	cli();
	initTWIMaster(F_CPU, F_SCL);
	initSPIMaster();
	initLCD();
	initTimer0();
	initPhotoCell(5.0);
	setT0DutyCycle(70);
	initKeypad();
	initRgbLed();
	initMotionDetector();
	initSolenoid();
	initHallEffect();
	initBell();
	initTimer1();
	initLog();
	initWatchdog();
	
	rgbClear();
	lcdClear();
	
	state = eeprom_read_word(&eeState);
	laststate = eeprom_read_word(&eeLastState);
	if (state == 5) {
		setState(0);
	}
	
	if (status) {
		cli();
		if (status & _BV(WDRF)) {
			lcdWriteString("WD RESET\n");
		} else if (status & _BV(BORF)) {
			lcdWriteString("BO RESET");
		} else if (status & _BV(EXTRF)) {
			lcdWriteString("EXT RESET");
		} else {
			lcdWriteString("PO RESET");
		}
		MCUSR = 0;
		sei();
	}
	
	char input;
	sei();
	
	while (1) {
		cli();
		input = getKey();
		sei();
		
		switch (input) {
			case '1':
				inputting = 1;
				if (pinPrompt()) {
					switchSolenoid(1);
					_delay_ms(3000);
					switchSolenoid(0);
					wdt_reset();
				}
				inputting = 0;
				break;
			case '3':
				inputting = 1;
				if (pinPrompt()) {
					_alarmOn = switchAlarm(0);
					enterLog(ALARM, 0, NA);
				}
				inputting = 0;
				break;
			case '7':
				// For use in testing the watchdog timer
				inputting = 1;
				cli();
				while (getKey() == '7');
				sei();
				inputting = 0;
				break;
			case '8':
				if (state == 0) {
					setState(5);
					mainMenu();
					setState(laststate);
				}
				break;
			case '#':
				inputting = 1;
				while (getKey() != 0) {
					wdt_reset();
				}
				if (pinPrompt()) {
					enterLog(ARMDISARM, 1, NA);
					_alarmOn = switchAlarm(0);
					setState(1);
				}
				inputting = 0;
				break;
			case '*':
				inputting = 1;
				if (pinPrompt()) {
					enterLog(ARMDISARM, 9, NA);
					if (_alarmOn) {
						enterLog(ALARM, 0, NA);
					}
					_alarmOn = switchAlarm(0);
					setState(0);
				}
				inputting = 0;
				break;
			default:
				break;
		}
		
		
		
		/* State mappings:
		 * 99 - fire alarm
		 *  0 - idle disarmed
		 *  1 - idle armed
		 *  2 - motion alarm
		 *  3 - door alarm
		 *  4 - window alarm
		 *  5 - menu
		 */
		switch (state) {
			case 0:
				if (laststate != 0) {
					rgbClear();
					rgbOn('g');
					if (laststate == 1) {
						enterLog(ARMDISARM, 0, NA); 
					}
				}
				break;
			case 1:
				if (laststate != 1) {
					rgbClear();
					rgbOn('r');
					if (laststate == 0) {
						enterLog(ARMDISARM, 1, NA);
					}
			}	
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
			case 5:
				break;
			case 99:
				break;
			default:
				lcdClear();
				lcdWriteString("ERROR:\n");
				lcdWriteString("INVLD STATE\n");
				break;
		}
	}
}


