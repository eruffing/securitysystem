/**
 * \file PIN.h
 *
 * \since 2015-09-25
 * \author Ethan Ruffing
 * 
 * \brief Utilities for managing a PIN
 *
 * This file contains a set of tools for inputting, tracking, and confirming a
 * PIN.
 */

#include <string.h>
#include <stdio.h>
#include <avr/eeprom.h>
#include "config.h"

#ifndef PIN_H_
#define PIN_H_

/**
 * Sets the PIN to the specified value.
 *
 * \param[in] pin
 *     The pin to set in memory
 */
void setPin(uint16_t pin);

/**
 * Gets the PIN that has been set.
 *
 * \returns
 *     The pin read from memory
 */
uint16_t getPin();

/**
 * Checks whether the given pin matches the stored pin.
 *
 * \param[in] inputPin
 *     The pin attempt to check.
 * \return
 *     True if the pins match, false if not.
 */
uint8_t checkPin(uint16_t inputPin);


#endif /* PIN_H_ */
