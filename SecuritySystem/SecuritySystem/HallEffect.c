/**
 * \file HallEffect.c
 *
 * \since 2015-11-25
 * \author Ethan Ruffing
 *
 * \brief Implementation of HallEffect.h
 *
 * This file contains implementations of the functions declared in HallEffect.h.
 */ 

#include "HallEffect.h"

void initHallEffect() {
	HE0_DDR &= ~_BV(HE0_BIT);	// Set as input
	HE0_PORT |= _BV(HE0_BIT);	// Enable pull-up
	HE1_DDR &= ~_BV(HE1_BIT);
	HE1_PORT |= _BV(HE1_BIT);
	
	PCICR |= _BV(PCIF2);
	PCMSK2 |= _BV(HE0_BIT) | _BV(HE1_BIT);
}

int hallEffectLatched() {
	return !(!(HE0_PIN & _BV(HE0_BIT)));
}
