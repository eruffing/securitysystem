/**
 * \file Test.c
 *
 * \since 2015-11-04
 * \author Ethan Ruffing
 *
 * \brief Implementation of Test.h
 *
 * This file contains implementations of the functions declared in Test.h.
 */ 

#include "Test.h"

void test() {
	initTWIMaster(F_CPU, F_SCL);
	initSPIMaster();
	initLCD();
	initTimer0();
	initPhotoCell(2, 5.0);
	setT0DutyCycle(70);
	initKeypad();
	initRgbLed();
	initMotionDetector();
	initSolenoid();
	initHallEffect();
	initBell();
	
	uint8_t motionAlarm = 0, solOn = 0, bell = 0;
	
	while (1) {
		lcdClear();
		printStatus();
		rgbClear();
		
		if (motionDetected()) {
			motionAlarm = 1;
		}

		
		float v = readVoltage();
		int dutyCycle = 100 - v * 25;	// Calculate duty cycle based on voltage
		dutyCycle = dutyCycle < 0 ? 0 : dutyCycle;
		dutyCycle = dutyCycle > 100 ? 100 : dutyCycle;
		setT0DutyCycle(dutyCycle);
		
		if (motionAlarm) {
			rgbOn('r');
			rgbOn('g');
		}
		
		if (hallEffectLatched()) {
			lcdWriteString("Door CLOSED\n");
		} else {
			lcdWriteString("Door OPEN\n");
		}
		
		lcdWriteString("Press a key.\n");
		char c = getKey();
		lcdWriteChar(c);
		switch (c) {
			case '1':
				rgbOn('r');
				break;
			case '2':
				rgbOn('g');
				break;
			case '3':
				motionAlarm = 0;
				break;
			case '4':
				solOn = 1;
				break;
			case '5':
				solOn = 0;
				break;
			case '6':
				bell = 1;
				break;
			case '7':
				bell = 0;
				break;
		}
		
		switchSolenoid(solOn);
		switchBell(bell);
		
		_delay_ms(100);
	}
}

void printStatus() {
	char output[48];
	uint8_t year, month, date, hour, minute, second, pm = 0;
	float temp;
	
	rtcReadDateTime(&hour, &minute, &second, &date, &month, &year);
	pm = hour >= 12;
	if (hour > 12) {
		hour -= 12;
	}
	lcdSetCursor(0, 0);
	if (pm) {
		sprintf(output, "%02d:%02d:%02d PM\n", hour, minute, second);
	} else {
		sprintf(output, "%02d:%02d:%02d AM\n", hour, minute, second);
	}
	lcdWriteString(output);
	sprintf(output, "%02d-%02d-%02d\n", month, date, year);
	lcdWriteString(output);
	temp = rtcReadTemperature();
	sprintf(output, "%3d.%02hd C\n", (int)temp, (int)((temp-(int)temp)*100));
	lcdWriteString(output);
}

void printDateTime(uint8_t hour, uint8_t minute, uint8_t second, uint8_t date,
                   uint8_t month, uint8_t year) {
	printf("%02d:%02d:%02d ", (int)hour, (int)minute, (int)second);
	printf("%02d-%02d-%02d", (int)month, (int)date, (int)year);
}
