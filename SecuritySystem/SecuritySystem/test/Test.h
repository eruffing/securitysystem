/**
 * \file Test.h
 *
 * \since 2015-11-04
 * \author Ethan Ruffing
 *
 * \brief A set of tools for system testing
 *
 * This header provides functions for unit testing the various components in the
 * system.
 *
 * Each test is entirely self-contained. To use one, simply call it as the first
 * and only function in `main()`.
 */ 

#ifndef F_CPU
#error F_CPU must be defined before including Test.h!
#endif
//#define BAUD 9600
//#define MYUBRR F_CPU/16/BAUD-1
#define F_SCL 100000UL
//
//#include "uart.h"
#include "../TWI.h"
#include "../Keypad.h"
#include "../PIN.h"
#include "../RTC.h"
#include "../Photocell.h"
#include "../LCD.h"
#include "../Timers.h"
#include "../RGB_LED.h"
#include "../MotionDetector.h"
#include "../Solenoid.h"
#include "../HallEffect.h"
#include "../Bell.h"
#include <util/delay.h>

#ifndef TEST_H_
#define TEST_H_

/**
 * Runs a test program on the microcontroller for testing the various peripherals.
 */
void test();

/**
 * Prints current status information to the first three rows of the LCD.
 */
void printStatus();

/**
 * Prints the formatted date and time to STDOUT.
 *
 * \param[in] hour
 *     The current hour
 * \param[in] minute
 *     The current minute
 * \param[in] second
 *     The current second
 * \param[in] date
 *     The current date
 * \param[in] month
 *     The current month
 * \param[in] year
 *     The current year
 */
void printDateTime(uint8_t hour, uint8_t minute, uint8_t second, uint8_t date,
                   uint8_t month, uint8_t year);

#endif /* TEST_H_ */
