/**
 * \file UI.h
 *
 * \since 2015-11-30
 * \author Ethan Ruffing
 *
 * \brief Implementation of UI.h
 *
 * This file contains implementations of the functions declared in UI.h.
 */ 

#include "UI.h"

const char daysOfWeek[7][5] = {
	"Sun\0",
	"Mon\0",
	"Tue\0",
	"Wed\0",
	"Thu\0",
	"Fri\0",
	"Sat\0"
};

uint16_t scand() {
	char in[20], inp;
	int i, x, y;
	
	for(i = 0; inp != '#'; i++) {
		x = cursorPos[0], y = cursorPos[1];
		
		while (getKey() != 0) {
			wdt_reset();
		}
		
		inp = getInput();
		in[i] = inp;
		cli();
		lcdSetCursor(x, y);
		lcdWriteChar(inp);
		sei();
	}
	while (getKey() != 0) {wdt_reset();}
	
	return (uint16_t)atoi(in);
}

void timeInput() {
	uint8_t year, month, date, hour, minute, second;
	lcdClear();
	lcdSetCursor(0, 1);
	lcdWriteString("Enter Year:\n");
	year = scand();
	lcdClear();
	lcdSetCursor(0, 1);
	lcdWriteString("Enter Month:\n");
	month = scand();
	lcdClear();
	lcdSetCursor(0, 1);
	lcdWriteString("Enter Date:\n");
	date = scand();
	lcdClear();
	lcdSetCursor(0, 1);
	lcdWriteString("Enter hour:\n");
	hour = scand();
	lcdClear();
	lcdSetCursor(0, 1);
	lcdWriteString("Enter min:\n");
	minute = scand();
	lcdClear();
	lcdSetCursor(0, 1);
	lcdWriteString("Enter sec:\n");
	second = scand();
	lcdClear();
	
	rtcSetDateTime(hour, minute, second, date, month, year);
}

void pinSet() {
	uint16_t old, new0, new1;
	while(getKey() != 0);
	
	// Prompt for old pin
	lcdClear();
	lcdSetCursor(0, 1);
	lcdWriteString("Old PIN:");
	old = scand();
	while(!checkPin(old)) {
		lcdClear();
		lcdSetCursor(0, 1);
		lcdWriteString("Incorrect.\n");
		lcdWriteString("Old PIN:");
		old = scand();
	}
	
	// Prompt for new pin
	lcdClear();
	lcdSetCursor(0, 1);
	lcdWriteString("New PIN:");
	new0 = scand();
	lcdWriteString("\nAgain:");
	new1 = scand();
	while (new0 != new1) {
		lcdClear();
		lcdSetCursor(0, 1);
		lcdWriteString("Match failed");
		lcdWriteString("New PIN: ");
		new0 = scand();
		lcdWriteString("\nAgain: ");
		new1 = scand();
	}
	
	setPin(new0);
}

uint8_t pinPrompt() {
	lcdClear();
	lcdSetCursor(0, 1);
	lcdWriteString("PIN:");
	uint16_t pin = scand();
	return checkPin(pin);
}

void printStatus(uint8_t state) {
	cli();
	
	lcdClear();
	lcdSetCursor(0, 1);
	switch (state) {
		case 0:
			lcdWriteString("  DISARMED");
			break;
		case 1:
			lcdWriteString("   ARMED");
			break;
		case 2:
			lcdWriteString(" Intruder!");
			break;
		case 3:
			lcdWriteString(" Door Open!");
			break;
		case 4:
			lcdWriteString("Window Open!");
			break;
		case 99:
			lcdWriteString("   FIRE!");
			break;
		default:
			lcdWriteString("ERROR:\n");
			lcdWriteString("INVLD STATE\n");
			break;
	}
	
	wdt_reset();
	
	sei();
}

void scrollStatus() {
	static uint8_t position = 0;
	uint8_t month, date, hour, minute, second, pm = 0;
	char status[42], output[13];
	uint16_t year;
	float temp;
	
	rtcRead();
	
	lcdClearRow(0);
	
	rtcReadDateTime(&hour, &minute, &second, &date, &month, &year);
	temp = rtcReadTemperature() * 1.8 + 32;
	
	pm = hour >= 12;
	if (hour > 12) {
		hour -= 12;
	}
	
	static int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
	year -= month < 3;
	uint8_t day = (year + year/4 - year/100 + year/400 + t[month-1] + date) % 7;
	
	if (pm) {
		sprintf(status, "%02d:%02d:%02d PM | ", hour, minute, second);
		} else {
		sprintf(status, "%02d:%02d:%02d AM | ", hour, minute, second);
	}
	
	strcpy(status + 14, daysOfWeek[day]);
	strcat(status, " ");
	
	sprintf(status + 18, "%02d-%02d-%02d |", month, date, year % 100);
	sprintf(status + 28, "%3d.%02hd F |", (int)temp,
	        (int)((temp-(int)temp)*100));
	
	// Prepare buffer
	strncpy(output, status + position, 12);
	if (strlen(output) < 12) {
		strcat(output, " ");
		strncpy(output + strlen(output), status,
		        12 - strlen(status + position) - 1);
	}
	
	output[12] = 0;
	lcdWriteString(output);
	
	position = position >= strlen(status) ? 0 : position + 1;
}

void printDateTime(uint8_t hour, uint8_t minute, uint8_t second, uint8_t date,
                   uint8_t month, uint8_t year) {
	printf("%02d:%02d:%02d ", (int)hour, (int)minute, (int)second);
	printf("%02d-%02d-%02d", (int)month, (int)date, (int)year);
}

void menu(const char title[], const char * items[], func * callbacks,
          uint8_t len, int * sel) {
	uint8_t i;
	
	while (getKey() != 0) {
		wdt_reset();
	}
	
	while (*sel != -1 && *sel != 6) {
		lcdClear();
		lcdSetCursor(0, 1);
		for(i = 0; i < len; i++) {
			if (*sel == i) {
				lcdWriteString("*");
				} else {
				lcdWriteString(" ");
			}
			lcdWriteString(items[i]);
			lcdWriteString("\n");
		}
		char k = getInput();
		while (getKey() != 0) {wdt_reset();}
		switch (k) {
			case '4':
				*sel = -1;
				break;
			case '2':
				*sel = (*sel == 0) ? len - 1 : *sel - 1;
				break;
			case '8':
				*sel = (*sel == len - 1) ? 0 : *sel + 1;
				break;
			case '6':
				callbacks[*sel]();
				while (getKey() != 0) {wdt_reset();}
				break;
		}
	}
}

void hallEffectView() {
	uint8_t door, window;
	
	while (getKey() != '4') {
		cli();
		window = HE0_PIN & _BV(HE0_BIT);
		door = HE1_PIN & _BV(HE1_BIT);
		
		lcdClear();
		lcdSetCursor(0, 1);
		lcdWriteString("Door:   ");
		lcdWriteChar(door ? 'O' : 'C');
		lcdSetCursor(0, 2);
		lcdWriteString("Window: ");
		lcdWriteChar(window ? 'O' : 'C');
		sei();
		
		wdt_reset();
		_delay_ms(525);
		wdt_reset();
	}
}

void motionView() {
	while (getKey() != '4') {
		cli();
		lcdClear();
		lcdSetCursor(0, 1);
		lcdWriteString("Motion:\n");
		lcdWriteString(motionDetected() ? "DETECTED" : "NOT DETECTED");
		sei();
		
		wdt_reset();
		_delay_ms(525);
		wdt_reset();
	}
}

void tempView() {
	char output[18];
	float temp;
	
	while (getKey() != '4') {
		temp = rtcReadTemperature();

		sprintf(output, "%3d.%02hd F\n", (int)temp, 
		        (int)((temp-(int)temp)*100));
		sprintf(output, "%3d.%02hd C", (int)temp, (int)((temp-(int)temp)*100));
		output[17] = 0;
	
		cli();
		lcdClear();
		lcdSetCursor(0, 1);
		lcdWriteString(output);
		sei();
		
		wdt_reset();
		_delay_ms(525);
		wdt_reset();
	}
}

void photocellView() {
	char output[19];
	float voltage;
	
	while (getKey() != '4') {
		voltage = readVoltage();
		
		sprintf(output, "Photocell:\n%2d.%02hd V", (int)voltage,
		        (int)((voltage-(int)voltage)*100));
		output[18] = 0;
		
		cli();
		lcdClear();
		lcdSetCursor(0, 1);
		lcdWriteString(output);
		sei();
		
		wdt_reset();
		_delay_ms(525);
		wdt_reset();
	}
}

void sensorMenu() {
	int sel = 0;
	
	// Sensor menu options. Each is < 11 characters in length.
	const char * sensorMenuOptions[] = {
		"Door/Window",
		"Presence",
		"Temperature",
		"Luminance"
	};
	const uint8_t sensorMenuLen = 4;
	
	func sensorMenuCallbacks[] = {
		*hallEffectView,
		*motionView,
		*tempView,
		*photocellView
	};
	
	menu("  SENSORS", sensorMenuOptions, sensorMenuCallbacks, sensorMenuLen,
	     &sel);
}

void alarmLogs() {
	uint8_t i;
	log_entry entry;
	char output[13];
	
	lcdClear();
	lcdSetCursor(0, 1);
	for(i = 0; i < LOG_LENGTH; i++) {
		readLogEntry(i, ALARM, &entry);
		sprintf(output, "%02d%02d%02d%02d", entry.datetime.month,
		        entry.datetime.date, entry.datetime.hour,
		        entry.datetime.minute);
		sprintf(output + strlen(output), " %c", (entry.activated ? 'S' : 'C'));
		switch (entry.source) {
			case DOOR:
				sprintf(output + strlen(output), " D");
				break;
			case FIRE:
				sprintf(output + strlen(output), " F");
				break;
			case MOTION:
				sprintf(output + strlen(output), " M");
				break;
			case WINDOW:
				sprintf(output + strlen(output), " W");
				break;
			case NA:
				sprintf(output + strlen(output), " U");
				break;
		}
		
		lcdSetCursor(0, 1 + i);
		lcdWriteString(output);
	}
	while (getKey() != '4') {
		wdt_reset();
	}
}

void armLogs() {
		uint8_t i;
		log_entry entry;
		char output[13];
		
		cli();
		lcdClear();
		lcdSetCursor(0, 1);
		for(i = 0; i < LOG_LENGTH; i++) {
			readLogEntry(i, ARMDISARM, &entry);
			sprintf(output, "%02d%02d %02d%02d", entry.datetime.month,
			entry.datetime.date, entry.datetime.hour,
			entry.datetime.minute);
			sprintf(output + strlen(output), " %c\n",
			        (entry.activated ? 'A' : 'D'));
			
			lcdSetCursor(0, 1 + i);
			lcdWriteString(output);
		}
		sei();
		while (getKey() != '4') {
			wdt_reset();
		}
}

void logMenu() {
	int sel = 0;
	
	// Log menu options
	const char * logMenuOptions[] = {
		"Alarms",
		"Arm/Disarm"
	};
	const uint8_t logMenuLen = 2;
	
	func logMenuCallbacks[] = {
		*alarmLogs,
		*armLogs
	};
	
	menu("    LOGS", logMenuOptions, logMenuCallbacks, logMenuLen, &sel);
}

void settingsMenu() {
	int sel = 0;
	
	// Settings menu options. Each is < 11 character in length.
	const char * settingsMenuOptions[] = {
		"Set clock",
		"Set pin"
	};
	const uint8_t settingsMenuLen = 4;
	
	func settingsMenuCallbacks[] = {
		*timeInput,
		*pinSet
	};
	
	menu("  SETTINGS", settingsMenuOptions, settingsMenuCallbacks,
	     settingsMenuLen, &sel);
}

void mainMenu() {
	int sel = 0;
	
	// Main menu options. Each is < 11 characters in length.
	const char * mainMenuOptions[] = {
		"Sensors",
		"View log",
		"Settings"
	};
	
	func a[] = {
		sensorMenu,
		logMenu,
		settingsMenu
	};
	
	const uint8_t mainMenuLen = 3;
	
	menu("MAIN MENU", mainMenuOptions, a, mainMenuLen, &sel);
}
