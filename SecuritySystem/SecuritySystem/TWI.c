/**
 * \file TWI.c
 *
 * \since 2015-10-02
 * \author Ethan Ruffing
 * 
 * \brief Implementation of TWI.h
 *
 * This file contains implementations of the functions declared in TWI.h.
 *
 * Note: This code is adapted from code provided by Bruce Wall in his article
 * "Add a DS1307 RTC clock to your AVR microcontroller" and from code provided
 * with the Atmel Application Note 315: "Using the TWI module as TWI master".
 */ 

#include "TWI.h"

void initTWIMaster(const unsigned long f_cpu, const unsigned long f_scl) {
	// Set prescaler to zero, as specified in Atmel Application Note 315
	DDRC |= _BV(5);
	TWSR = 0;
	
	// Set SCL frequency using appropriate calculated value
	TWBR = ((f_cpu/(float)f_scl) - 16) / 2;
}

int twiSendStart() {
	TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);	// Send start condition
	while (!(TW_READY));	// Wait
	return (TW_STATUS == 0x08); // Return 1 if successful, 0 if not
}

void twiSendStop() {
	TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
}

int twiSendAddress(uint8_t addr) {
	TWDR = addr;
	TWCR = _BV(TWINT) | _BV(TWEN);
	while (!TW_READY);
	return (TW_STATUS == 0x18);
}

int twiSendByte(uint8_t data) {
	TWDR = data;
	TWCR = _BV(TWINT) | _BV(TWEN);
	while (!TW_READY);
	return (TW_STATUS != 0x28);
}

uint8_t twiReadACK() {
	TWCR = _BV(TWINT) | _BV(TWEA) | _BV(TWEN);
	while (!TW_READY);
	return TWDR;
}

uint8_t twiReadNACK() {
	TWCR = _BV(TWINT) | _BV(TWEN);
	while (!TW_READY);
	return TWDR;
}
