/**
 * \file Timers.c
 *
 * \since 2015-09-30
 * \author Ethan Ruffing
 * 
 * \brief Implementation of Timers.h
 *
 * This file contains implementations of the functions declared in Timers.h.
 */

#include "Timers.h"

void initTimer0() {
	DDRD |= _BV(5);
	PORTB &= ~_BV(5);
	TCCR0A =  _BV(COM0B1);
	TCCR0A |= _BV(WGM00);
	TCCR0B = _BV(WGM02);
	TCCR0B |= _BV(CS02);
	
	OCR0A = 156;
	setT0DutyCycle(DEFAULT_DUTYCYCLE);
}

void setT0DutyCycle(uint8_t dutyCycle) {
	unsigned int d = (int)(1.56*dutyCycle);
	OCR0B =  d;
}

void initTimer1() {
	TCCR1B = _BV(CS12);	// Set prescaler to divide-by-256
	TCCR1A = 0;
	
	OCR1A = 31250;
	TCNT1 = 0;
	//TIFR1 = _BV(OCF1A);
	
	TIMSK1 |= _BV(OCIE1A);
}

void initTimer2() {
	TCCR2B = _BV(CS12) | _BV(CS11) | _BV(CS10);	// Set prescaler to divide-by-1024
	TCCR2A = 0;
	
	TIMSK2 = _BV(TOIE2);
}
