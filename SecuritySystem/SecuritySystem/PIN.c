/**
 * \file PIN.c
 *
 * \since 2015-09-25
 * \author Ethan Ruffing
 * 
 * \brief Implementation of PIN.h
 * 
 * This file contains implementations of the functions declared in PIN.h.
 */

#include "PIN.h"

void setPin(uint16_t pin) {
	eeprom_update_word(&eePin, pin);
}

uint16_t getPin() {
	return eeprom_read_word(&eePin);
}

uint8_t checkPin(uint16_t inputPin) {
	return getPin() == inputPin;
}
