/**
 * \file Bell.c
 *
 * \since 2015-11-25
 * \author Ethan Ruffing
 */ 

#include "Bell.h"

void initBell() {
	BELL_DDR |= _BV(BELL_BIT);
	BELL_PORT &= ~_BV(BELL_BIT);
}

void switchBell(int ringing) {
	if (ringing == 2) {
		if (BELL_PORT & _BV(BELL_BIT)) {
			BELL_PORT |= _BV(BELL_BIT);
		} else {
			BELL_PORT &= ~_BV(BELL_BIT);
		}
	} else if (ringing) {
		BELL_PORT |= _BV(BELL_BIT);
	} else {
		BELL_PORT &= ~_BV(BELL_BIT);
	}
}
