Security System
===============
Copyright 2015 by Ethan Ruffing and Bashayer Alhanini

This is the software for a home security system, designed for the final project
in EGR 326 during the fall 2015 semester at Grand Valley State University's
Padnos College of Engineering and Computing.

It contains several utilities to speed up development of the assigned mock home
security system by preconfiguring drivers for the various peripherals necessary.

Detailed documentation can be generated using Doxygen in the root directory.
