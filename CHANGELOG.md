Changelog
=========
## 1.0.1 ##
* Final version for submission

## 1.0.0 ##
* Initial release; version used in final graded demonstration
